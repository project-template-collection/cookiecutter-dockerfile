======================
Cookiecutter Simple Template for creating Dockerimage Projects
======================


Features
--------

* Generiert eine Struktur für ein neues DockerImage Projekt


.. code-block:: shell

  .
  └── docker-boilerplate
      ├── Dockerfile
      └── README.rst

  1 directory, 2 files


Usage
--------


.. code-block:: shell

  source ~/development/lib/virtualenv/cookiecutter/bin/activate
  cookiecutter git+ssh://git@bill:10022/configuration/cookiecutter-project-templates/docker-image.git


Build
--------

  Generate the [sphinx-doc](http://www.sphinx-doc.org/en/master/index.html) and execute some ``pyTest``

.. code-block:: Shell
   :linenos:

   source ~/development/lib/virtualenv/tox/bin/activate
   tox

Open the Documentation

.. code-block:: Shell
    :linenos:

    browse .tox/docs/tmp/html/index.html &
