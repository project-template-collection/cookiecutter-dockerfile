======================
Dockerimage {{ cookiecutter.project_name }}
======================


Features
--------

* generate a project structure


Build
--------


.. code-block:: Shell
   :linenos:

   docker build -t {{ cookiecutter.docker_image_name }} .
   docker tag {{ cookiecutter.docker_image_name }} {{ cookiecutter.docker_image_version }}


Using Tox Build Script
^^^^^^^

.. code-block:: Shell
   :linenos:

   source ~/development/lib/virtualenv/tox/bin/activate
   tox
