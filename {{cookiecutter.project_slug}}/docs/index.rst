Welcome to {{cookiecutter.project_name}} documentation!
==================================================

Getting Started
---------------

.. toctree::
   :maxdepth: 2

   usage
   readme


Basics
------

.. toc   console_script_setup   console_script_setuptree::
   :maxdepth: 2

   usage

Advanced Features
-----------------

.. toctree::
   :maxdepth: 2

   usage



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
